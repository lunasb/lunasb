
```ruby
 CURRENTLY LISTENING TO
```
<div align="left">
  <a href="https://open.spotify.com/user/sushigameplay03?si=b8d3ddb4f0574e41" target="_blank">
    <img src="https://spotify-readme-git-main-lunas-projects-1ac9c12c.vercel.app/api?theme=dark" alt="Current Spotify Song" width="50%">
  </a>
</div>

<br>

---

<br>

```lex
 ABOUT ME
```

```python
class LunaMellow():

    def set_about_me(self):
        self.name = "Luna Sofie"
        self.username = "LunaMellow"
        self.location = "Norway"
        self.hobby = "Virtual Reality"

    def set_work(business):
        business.name = "LUNAS"
        business.role = "CEO and Founder"
        business.github = "@LS-AS"
        business.website = "https://lunaservices.no"

    def set_education(study):
        study.university = "NTNU"
        study.name = "Bachelor's Degree in Programming"
        study.location = "Gjøvik, Norway"
        study.start = "August 2023"
        study.website = "https://www.ntnu.no"

    def __str__(self):
        return self.name

if __name__ == '__main__':
    about_me = LunaMellow()
    about_me.set_about_me()

    work_me = LunaMellow()
    work_me.set_work()

    education_me = LunaMellow()
    education_me.set_education()

```
